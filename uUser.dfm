object fmUser: TfmUser
  Left = 0
  Top = 0
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsSingle
  Caption = 'Seja bem-vindo'
  ClientHeight = 444
  ClientWidth = 515
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 515
    Height = 145
    Align = alTop
    Color = clMaroon
    ParentBackground = False
    TabOrder = 0
    ExplicitWidth = 505
  end
  object Panel2: TPanel
    Left = 0
    Top = 136
    Width = 515
    Height = 308
    Align = alBottom
    Color = clHighlightText
    ParentBackground = False
    TabOrder = 1
    object btSacar: TButton
      Left = 168
      Top = 16
      Width = 185
      Height = 41
      Caption = 'Sacar'
      TabOrder = 0
      OnClick = btSacarClick
    end
    object btSaldo: TButton
      Left = 168
      Top = 80
      Width = 185
      Height = 41
      Caption = 'Saldo'
      TabOrder = 1
      OnClick = btSaldoClick
    end
    object btSair: TButton
      Left = 168
      Top = 144
      Width = 185
      Height = 41
      Caption = 'Sair'
      TabOrder = 2
      OnClick = btSairClick
    end
  end
end
