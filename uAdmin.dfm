object fmAdmin: TfmAdmin
  Left = 0
  Top = 0
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'Modo Administrador'
  ClientHeight = 444
  ClientWidth = 515
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 515
    Height = 97
    Align = alTop
    Color = clMaroon
    ParentBackground = False
    TabOrder = 0
  end
  object Panel2: TPanel
    Left = 0
    Top = 96
    Width = 515
    Height = 348
    Align = alBottom
    Color = clHighlightText
    ParentBackground = False
    TabOrder = 1
    object btCadastrar: TButton
      Left = 152
      Top = 37
      Width = 217
      Height = 41
      Caption = 'Cadastrar/Editar Usu'#225'rios'
      TabOrder = 0
      OnClick = btCadastrarClick
    end
    object btInserir: TButton
      Left = 152
      Top = 100
      Width = 217
      Height = 39
      Caption = 'Inserir'
      TabOrder = 1
      OnClick = btInserirClick
    end
    object btExibir: TButton
      Left = 152
      Top = 161
      Width = 217
      Height = 37
      Caption = 'Exibir'
      TabOrder = 2
      OnClick = btExibirClick
    end
    object btSair: TButton
      Left = 152
      Top = 220
      Width = 217
      Height = 41
      Caption = 'Sair'
      TabOrder = 3
      OnClick = btSairClick
    end
  end
end
