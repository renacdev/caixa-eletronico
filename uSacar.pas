unit uSacar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TfmSacar = class(TForm)
    btSacar: TButton;
    edSacar: TEdit;
    Label1: TLabel;
    btCancelar: TButton;
    procedure btSacarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSacar: TfmSacar;

implementation

uses uInserir;

{$R *.dfm}

procedure TfmSacar.btCancelarClick(Sender: TObject);
begin
    fmSacar.Close;
end;

procedure TfmSacar.btSacarClick(Sender: TObject);
var
  wl_saque  : Integer;
begin
  wl_saque := (StrToInt(edSacar.Text));

  if wl_saque > wg_saldo then
  showMessage('Saldo Insuficiente')
  else
  wg_saldo := wg_saldo - wl_saque;
  showMessage('Seu saldo �: R$'+(IntToStr(wg_saldo))+',00');

end;

procedure TfmSacar.FormCreate(Sender: TObject);
begin
    showMessage('Seu saldo �: R$'+(IntToStr(wg_saldo))+',00');
end;

end.
