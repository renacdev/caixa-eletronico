unit uClientes;

interface

type
  Tclientes = class(TObject)
  private
    FNome    : String;
    FNumero  : string;
    FTipo    : String;
    FLogin   : String;
    FSenha   : String;

  public
    Constructor Create(p_nome, p_numero,p_tipo,p_login,p_senha : string);
    property Nome : String read FNome write FNome;
    property Numero : String read FNumero write FNumero;
    property Tipo : String read FTipo write FTipo;
    property Login : String read FLogin write FLogin;
    property Senha : String read FSenha write FSenha;
  end;


 var wg_clientes : Array of Tclientes;
 procedure AddCliente (p_nome, p_numero, p_tipo,  p_login, p_senha: string);


implementation

{ Tclientes }

constructor Tclientes.Create(p_nome, p_numero, p_tipo, p_login, p_senha: string);
begin
  inherited Create();
  Nome     := p_nome;
  Numero := p_numero;
  Tipo    := p_tipo;
  Login    := p_tipo;
  Senha    := p_tipo;
end;


procedure AddCliente (p_nome, p_numero, p_tipo, p_login, p_senha: string);
var
  wl_pos: Integer;
  wl_clt: Tclientes;
 begin
   SetLength(wg_clientes, length(wg_clientes)+1);
   wl_pos := High(wg_clientes);
   wl_clt := Tclientes.Create(p_nome, p_numero, p_tipo, p_login, p_senha);
   wg_clientes[wl_pos] := wl_clt;
end;

end.
