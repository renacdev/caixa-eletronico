object fmInserir: TfmInserir
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = 'Inserir Notas'
  ClientHeight = 204
  ClientWidth = 430
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 78
    Top = 39
    Width = 89
    Height = 18
    Caption = 'Valor da Nota'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 29
    Top = 87
    Width = 138
    Height = 18
    Caption = 'Quantidade de Notas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object edQuant: TEdit
    Left = 181
    Top = 84
    Width = 145
    Height = 21
    TabOrder = 1
    OnKeyPress = edQuantKeyPress
  end
  object btOk: TButton
    Left = 208
    Top = 128
    Width = 75
    Height = 25
    Caption = 'Ok'
    TabOrder = 2
    OnClick = btOkClick
  end
  object cbValor: TComboBox
    Left = 181
    Top = 40
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 0
    OnKeyPress = cbValorKeyPress
    Items.Strings = (
      '10'
      '20'
      '50'
      '100')
  end
end
