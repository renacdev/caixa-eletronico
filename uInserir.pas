unit uInserir;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TfmInserir = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    edQuant: TEdit;
    btOk: TButton;
    cbValor: TComboBox;
    procedure btOkClick(Sender: TObject);
    procedure edQuantKeyPress(Sender: TObject; var Key: Char);
    procedure cbValorKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmInserir  : TfmInserir;
  wg_cb_valor: Integer;
  wg_ed_quant: integer;
  wg_saldo   : Integer;


implementation

{$R *.dfm}

procedure TfmInserir.btOkClick(Sender: TObject);
begin

//Valida��o campo tem que ser preenchido
  if cbValor.Text = '' then
  begin
    showmessage('Selecione um valor de nota v�lido');
    cbValor.SetFocus;
    exit;
  end;

  if edQuant.Text = '' then
  begin
    showmessage('Selecione uma quantidade v�lida');
    edQuant.SetFocus;
    exit;
  end;
//*****************************************

//Valida��o quantidade de notas deve ser maior que 0
  if edQuant.Text = '0' then
  begin
    showmessage('Selecione uma quantidade v�lida');
    edQuant.SetFocus;
    exit;
  end;
//*****************************************
  wg_cb_valor := (StrToInt(cbValor.Text));
  wg_ed_quant := (StrToInt(edQuant.Text));

  wg_saldo:= wg_cb_valor*wg_ed_quant;

  //erro resetar o saldo

  showmessage('Saldo Cadastrado');
  close;
end;

//Valida��o apenas Setas do teclado no edit
procedure TfmInserir.cbValorKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in [#26..#28]) then
  key := #0;
end;
//*********************************************************

//Valica��o digitar apenas n�meros no edit
procedure TfmInserir.edQuantKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9']) then
  key := #0;
end;
//*********************************************************
end.
