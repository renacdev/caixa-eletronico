object fmCadastrar: TfmCadastrar
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Cadastrar Conta'
  ClientHeight = 407
  ClientWidth = 428
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Image1: TImage
    Left = 0
    Top = 0
    Width = 428
    Height = 407
    Align = alClient
    ExplicitLeft = 1
  end
  object lbTitulo: TLabel
    Left = 175
    Top = 50
    Width = 98
    Height = 19
    Caption = 'Criar Conta '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label1: TLabel
    Left = 149
    Top = 67
    Width = 140
    Height = 14
    Caption = #201' Gratuito e sempre ser'#225'!'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lbNome: TLabel
    Left = 119
    Top = 143
    Width = 27
    Height = 13
    Caption = 'Nome'
  end
  object lbUsuario: TLabel
    Left = 75
    Top = 191
    Width = 74
    Height = 13
    Caption = 'Tipo de Usuario'
  end
  object lbLogin: TLabel
    Left = 122
    Top = 240
    Width = 25
    Height = 13
    Caption = 'Login'
  end
  object lbSenha: TLabel
    Left = 119
    Top = 280
    Width = 30
    Height = 13
    Caption = 'Senha'
  end
  object rbAdmin: TRadioButton
    Left = 168
    Top = 181
    Width = 113
    Height = 17
    Caption = 'Administrador'
    TabOrder = 1
    TabStop = True
  end
  object rbUser: TRadioButton
    Left = 168
    Top = 204
    Width = 113
    Height = 17
    Caption = 'Cliente'
    TabOrder = 2
    TabStop = True
  end
  object btnCadastrar: TButton
    Left = 187
    Top = 326
    Width = 75
    Height = 25
    Caption = 'Cadastrar'
    TabOrder = 5
    OnClick = btnCadastrarClick
  end
  object edNome: TDBEdit
    Left = 168
    Top = 140
    Width = 144
    Height = 21
    DataField = 'Nome'
    DataSource = DM.dsUsuarios
    TabOrder = 0
  end
  object edLogin: TDBEdit
    Left = 168
    Top = 237
    Width = 144
    Height = 21
    DataField = 'Login'
    DataSource = DM.dsUsuarios
    TabOrder = 3
  end
  object edSenha: TDBEdit
    Left = 168
    Top = 277
    Width = 144
    Height = 21
    DataField = 'Senha'
    DataSource = DM.dsUsuarios
    PasswordChar = '*'
    TabOrder = 4
  end
  object edTipoUsuario: TDBEdit
    Left = 8
    Top = 8
    Width = 121
    Height = 21
    DataField = 'TipoUsuario'
    DataSource = DM.dsUsuarios
    TabOrder = 6
    Visible = False
  end
  object edNumeroConta: TDBEdit
    Left = 8
    Top = 35
    Width = 121
    Height = 21
    DataField = 'NumeroConta'
    DataSource = DM.dsUsuarios
    TabOrder = 7
    Visible = False
  end
  object edSaldo: TDBEdit
    Left = 8
    Top = 62
    Width = 121
    Height = 21
    DataField = 'Saldo'
    DataSource = DM.dsUsuarios
    TabOrder = 8
    Visible = False
  end
end
