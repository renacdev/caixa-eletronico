unit udmDados;

interface

uses
  SysUtils, Classes, WideStrings, FMTBcd, DBClient, Provider, DB, SqlExpr, 
  Dialogs;

type
  TDM = class(TDataModule)
    sqlConn: TSQLConnection;
    sqlUsuarios: TSQLDataSet;
    dpsUsuarios: TDataSetProvider;
    cdsUsuarios: TClientDataSet;
    dsUsuarios: TDataSource;
    dsLogin: TDataSource;
    cdsLogin: TClientDataSet;
    dspLogin: TDataSetProvider;
    sqlLogin: TSQLDataSet;
    sqlUsuariosCOD_User: TIntegerField;
    sqlUsuariosNome: TStringField;
    sqlUsuariosNumeroConta: TIntegerField;
    sqlUsuariosTipoUsuario: TStringField;
    sqlUsuariosLogin: TStringField;
    sqlUsuariosSenha: TStringField;
    sqlUsuariosSaldo: TIntegerField;
    DataSetProvider1: TDataSetProvider;
    cdsUsuariosCOD_User: TIntegerField;
    cdsUsuariosNome: TStringField;
    cdsUsuariosNumeroConta: TIntegerField;
    cdsUsuariosTipoUsuario: TStringField;
    cdsUsuariosLogin: TStringField;
    cdsUsuariosSenha: TStringField;
    cdsUsuariosSaldo: TIntegerField;
    procedure cdsUsuariosNewRecord(DataSet: TDataSet);
    procedure cdsUsuariosReconcileError(DataSet: TCustomClientDataSet;
      E: EReconcileError; UpdateKind: TUpdateKind;
      var Action: TReconcileAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM: TDM;

implementation

{$R *.dfm}

procedure TDM.cdsUsuariosNewRecord(DataSet: TDataSet);
begin
  //cdsUsuariosCOD_User.Value:=cdsUsuarios.RecordCount+1;
end;

procedure TDM.cdsUsuariosReconcileError(DataSet: TCustomClientDataSet;
  E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
  showMessage(E.Message);
end;

end.
