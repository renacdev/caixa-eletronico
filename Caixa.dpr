program Caixa;

uses
  Forms,
  uLogin in 'uLogin.pas' {fmLogin},
  uAdmin in 'uAdmin.pas' {fmAdmin},
  uUser in 'uUser.pas' {fmUser},
  uCadastrar in 'uCadastrar.pas' {fmCadastrar},
  uInserir in 'uInserir.pas' {fmInserir},
  uSacar in 'uSacar.pas' {fmSacar},
  uClientes in 'uClientes.pas',
  uListar in 'uListar.pas' {fmListar},
  udmDados in 'udmDados.pas' {dmDados: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  //Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfmLogin, fmLogin);
  Application.CreateForm(TDM, DM);
  Application.Run;
end.
