unit uUser;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, uInserir;

type
  TfmUser = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    btSacar: TButton;
    btSaldo: TButton;
    btSair: TButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btSairClick(Sender: TObject);
    procedure btSaldoClick(Sender: TObject);
    procedure btSacarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmUser: TfmUser;

implementation

uses uLogin, uSacar;

{$R *.dfm}

procedure TfmUser.btSacarClick(Sender: TObject);
begin
   try
    fmSacar:=TfmSacar.Create(Self);
  finally
    fmSacar.ShowModal;
  end;
    fmSacar.Free;
end;

procedure TfmUser.btSairClick(Sender: TObject);
begin
  fmUser.Close;
  fmLogin.Show;
end;

procedure TfmUser.btSaldoClick(Sender: TObject);
begin
  showMessage('R$ '+ (IntToStr(wg_saldo)) +',00');
end;

procedure TfmUser.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  fmLogin.Show;
end;

end.
