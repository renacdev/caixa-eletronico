object fmSacar: TfmSacar
  Left = 0
  Top = 0
  BorderIcons = [biMinimize, biMaximize]
  Caption = 'Sacar'
  ClientHeight = 133
  ClientWidth = 430
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 100
    Top = 40
    Width = 86
    Height = 16
    Caption = 'Valor do saque'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object btSacar: TButton
    Left = 65
    Top = 82
    Width = 121
    Height = 28
    Caption = 'Sacar'
    TabOrder = 1
    OnClick = btSacarClick
  end
  object edSacar: TEdit
    Left = 192
    Top = 39
    Width = 121
    Height = 21
    TabOrder = 0
  end
  object btCancelar: TButton
    Left = 233
    Top = 82
    Width = 121
    Height = 28
    Caption = 'Sair'
    TabOrder = 2
    OnClick = btCancelarClick
  end
end
