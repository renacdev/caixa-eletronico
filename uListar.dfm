object fmListar: TfmListar
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Usu'#225'rios'
  ClientHeight = 509
  ClientWidth = 794
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object btEditar: TSpeedButton
    Left = 0
    Top = 465
    Width = 250
    Height = 45
    Caption = 'Editar'
    Glyph.Data = {
      36040000424D3604000000000000360000002800000010000000100000000100
      2000000000000004000000000000000000000000000000000000000000004E2C
      166B894A23BCAE5F2EEDB4632EF7B4632EF7B4632EF7B4622EF7B4622EF7B362
      2DF7B3622DF7B3612DF7AD5E2BEF884921BD472612630000000000000000A45B
      2CDEEBE5DEF2F5EADDFDF6EBDEFFF6EADEFFF6EADCFFF6EADCFFFAF3EBFFFAF3
      EBFFFAF2EAFFFCF7F3FFFAF6F2FDEFEFEEF0995023D50000000000000000B86D
      36F5F4EADEFEFDBF68FFFCBD67FFFBBE65FFFCBE64FFFCBE64FFFCBD62FFFBBD
      63FFFBBC61FFFCBE60FFFCBC62FFFBF9F6FDB05F2BF30000000000000000BB74
      3AF7F7EDE3FFFDC26EFF184257FF2B6187FF4C89BCFF709FB3FFE3C99AFFFFD6
      95FFFFD594FFFFD493FFFBBE65FFFBF7F4FFB5642FF70000000000000000BE78
      3EF7F7F0E6FFF8B455FF2E6682FF94C7F9FF91C9F9FF4185C9FF2668A6FFD2A8
      65FFF7B251FFF7B24FFFF7B24FFFFCF9F5FFB96C34F70000000000000000BF7C
      40F7F8F1E8FFFEE5D5FF4389AAFFE0F2FFFF549AD8FF1A7ABEFF4998C5FF488C
      C2FFDAD2CDFFFBE0C9FFFBE1C8FFFDFAF7FFBB7239F70000000000000000BF7E
      43F7F8F2EBFFFEE7D6FFA6B6BFFF7AB6D5FF90B7D1FF55C9E4FF5BDFF5FF78D0
      EDFF519BD9FFE1D6CDFFFBE1C9FFFBF7F2FFBF783DF70000000000000000C080
      45F7F9F3ECFFFEE8D6FFFEE8D7FFB3C6CCFF76B9D6FFC2F6FDFF63DFF7FF5DE2
      F8FF79D3F0FF4998DAFFE2D5C8FFFAF2EAFFC07C40F70000000000000000C084
      47F7F9F4EDFFFEE8D8FFFEE8D8FFFEE8D7FFB0C6CCFF77CBE7FFC7F7FDFF5EDC
      F5FF5AE1F7FF7BD4F1FF4B99DBFFD2DFE9FFC07E43F70000000000000000C084
      48F7F9F4EFFFFEE7D7FFFDE7D6FFFDE7D5FFFDE6D4FFBDD6D5FF79D3EEFFC7F7
      FDFF5FDCF5FF5BE2F7FF7AD6F2FF51A1E0FFA9825EF90000000000000000C085
      49F7F9F4F0FFFCE6D3FFFCE6D4FFFDE7D3FFFCE4D1FFFBE3CDFFBED4D0FF7DD4
      EEFFC4F6FDFF6CDDF6FF6DCAEDFF63A3D7FF6498C7FE0C161E2600000000C085
      49F7F9F5F1FFFCE3CFFFFBE4D0FFFCE4CFFFFCE3CDFFFAE1CAFFF9DDC4FFAFCD
      C9FF81D5EEFFB2E3F9FF8BC0E7FFAED3F6FFC4E0FCFF639ACCF700000000BF84
      49F6F9F5F1FFFCE3CDFFFBE3CEFFFBE3CDFFFBE2CBFFF9E0C8FFF8DCC2FFF5D6
      BAFFAFE3F1FF77BEE7FFB4D2F0FFE5F3FFFFACD2EFFF427FB5E800000000B57D
      45EAF7F3EFFCFAE0C7FFFBE1C9FFFBE2C9FFFBE0C8FFF9DFC5FFF8DBC1FFF4D6
      B8FFFFFBF8FFB6CBC2FF58A5D8FF85B1DBFF469DD0FF10374D5E000000009666
      38C3E5E0DAECF5F1EBFCF8F4EDFFF8F3EDFFF8F3EDFFF8F3EDFFF8F2ECFFF7F2
      ECFFF2E6D7FFE2B27DFFD28E61F505030207000000000000000000000000492F
      1A60936638BBBB834AEEC1874CF6C2884DF7C2884DF7C2894DF7C3884DF7C187
      4DF7A4723ED46F43229104020106000000000000000000000000}
    OnClick = btEditarClick
  end
  object btDelete: TSpeedButton
    Left = 544
    Top = 465
    Width = 250
    Height = 45
    Caption = 'Delete'
    Glyph.Data = {
      36040000424D3604000000000000360000002800000010000000100000000100
      2000000000000004000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000093FF000093FF000000000000000000000000000000000000
      000000000000000093FF000093FF000000000000000000000000000000000000
      000000009BFF0033FFFF0033FFFF000093FF0000000000000000000000000000
      0000000093FF0033FFFF0033FFFF000093FF0000000000000000000000000000
      93FF0029FFFF0029FFFF0033FFFF0033FFFF000093FF00000000000000000000
      93FF0033FFFF0033FFFF0033FFFF0033FFFF000093FF00000000000000000029
      CAFFFFCCFFFF0029FFFF0029FFFF0033FFFF0033FFFF000093FF000093FF0033
      FFFF0033FFFF0033FFFF0029FFFFFFCCFFFF0029CAFF00000000000000000000
      00000029CAFFFFCCFFFF0029FFFF0033FFFF0033FFFF0033FFFF0033FFFF0033
      FFFF0033FFFF0029FFFFFFCCFFFF0029CAFF0000000000000000000000000000
      0000000000000029CAFFFFCCFFFF0033FFFF0033FFFF0033FFFF0033FFFF0033
      FFFF0033FFFFFFCCFFFF0029CAFF000000000000000000000000000000000000
      000000000000000000000029CAFF023AFFFF023AFFFF023AFFFF023AFFFF023A
      FFFF023AFFFF0029CAFF00000000000000000000000000000000000000000000
      00000000000000000000000093FF0C4CFFFF0C4CFFFF0C4CFFFF0C4CFFFF0C4C
      FFFF0C4CFFFF000093FF00000000000000000000000000000000000000000000
      000000000000000093FF1560FFFF1560FFFF0B5AFFFF0B5AFFFF0B5AFFFF0B5A
      FFFF1560FFFF1560FFFF000093FF000000000000000000000000000000000000
      0000000093FF1E73FFFF1E73FFFF146CFFFF146CFFFFFFCCFFFFFFCCFFFF146C
      FFFF146CFFFF1E73FFFF1E73FFFF000093FF0000000000000000000000000000
      93FF2083FFFF2083FFFF1C7EFFFF1C7EFFFFFFCCFFFF0029CAFF0029CAFFFFCC
      FFFF1C7EFFFF1C7EFFFF2083FFFF2083FFFF000093FF00000000000000000029
      CAFFFFCCFFFF2083FFFF2083FFFFFFCCFFFF0029CAFF00000000000000000029
      CAFFFFCCFFFF2083FFFF2083FFFFFFCCFFFF0029CAFF00000000000000000000
      00000029CAFFFFCCFFFFFFCCFFFF0029CAFF0000000000000000000000000000
      00000029CAFFFFCCFFFFFFCCFFFF0029CAFF0000000000000000000000000000
      0000000000000029CAFF0029CAFF000000000000000000000000000000000000
      0000000000000029CAFF0029CAFF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000}
    OnClick = btDeleteClick
  end
  object Panel1: TPanel
    Left = -2
    Top = 64
    Width = 796
    Height = 44
    ParentBackground = False
    TabOrder = 1
    object Label1: TLabel
      Left = 8
      Top = 9
      Width = 377
      Height = 13
      Caption = 
        'Selecione o us'#250'ario e clique com o mouse para edit'#225'-lo ou exclui' +
        '-lo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 8
      Top = 23
      Width = 475
      Height = 13
      Caption = 
        'TipoUsuario '#39'0'#39' '#233' referente ao Administrador enquanto TipoUsuari' +
        'o '#39'1'#39' '#233' referente ao cliente comum'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Panel2: TPanel
      Left = 80
      Top = -392
      Width = 185
      Height = 41
      Caption = 'Panel2'
      TabOrder = 0
    end
    object Panel3: TPanel
      Left = 136
      Top = -408
      Width = 185
      Height = 41
      Caption = 'Panel3'
      TabOrder = 1
    end
    object Button1: TButton
      Left = 432
      Top = -32
      Width = 75
      Height = 25
      Caption = 'Button1'
      TabOrder = 2
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 105
    Width = 794
    Height = 360
    TabStop = False
    Align = alCustom
    DataSource = DM.dsUsuarios
    ReadOnly = True
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnDblClick = DBGrid1DblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'Nome'
        Width = 261
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NumeroConta'
        Width = 325
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TipoUsuario'
        Width = 172
        Visible = True
      end>
  end
  object Panel4: TPanel
    Left = 0
    Top = 0
    Width = 794
    Height = 65
    Align = alTop
    Color = clMaroon
    ParentBackground = False
    TabOrder = 2
    ExplicitWidth = 596
    object Label3: TLabel
      Left = 8
      Top = 13
      Width = 32
      Height = 13
      Caption = 'Nome'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 204
      Top = 13
      Width = 97
      Height = 13
      Caption = 'N'#250'mero da Conta'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 341
      Top = 13
      Width = 87
      Height = 13
      Caption = 'Tipo de Usu'#225'rio'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btPestodos: TSpeedButton
      Left = 648
      Top = 28
      Width = 25
      Height = 25
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000000000000000
        00000000000000000021006778CB001215560000002C00000027000000210000
        001A000000130000000D00000007000000030000000000000000000000000000
        0000000000000000001100495590006172BC000F123800000014000000110000
        000D0000000A0000000700000004000000020000000000000000000000000000
        00000000000000000000002C345304788ACE016070B2000A0B18000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000000F111B037687C815A9BBE5005A69A30007090D0000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000005D6BA126C6D6F2159FB1DE005461910003
        0305000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000003E476821A4B5DF3DD8E8FF1692A4D4004A
        557C000101010000000000000000000000000000000000000000000000000000
        0000000000000000000000000000001F2434148898CA4AD1E0FF4CD0DFFE1488
        99CB004049680000000000000000000000000000000000000000000000000000
        000000000000027788C0027384C3026E80C6016C7EC85BCFDDFF45C5D6FF5ACB
        D9FB108292C400353D5400000000000000000000000000000000000000000000
        00000000000000444E6B48AEBBDA8AEAF5FF7EE5F2FF7EE5F2FF7DE5F2FF7EE5
        F2FF7BDDE8F80F8191BE00292E3F000000000000000000000000000000000000
        0000000000000014161E0F8090BA8EEAF4FE66DDEEFF84E7F3FF017586B40175
        86B4017586B4017586B4017586B4000000000000000000000000000000000000
        000000000000000000000156638269C5CFE371E1F0FF79E4F2FF7ACFD8F10273
        84C2001F23300000000000000000000000000000000000000000000000000000
        0000000000000000000000242A3623919FC099EFF9FF72E3F4FF83E8F6FF81D5
        DDF002798BBE011F233000000000000000000000000000000000000000000000
        00000000000000000000000102020167769690DDE4ED7FE9FAFF7EE9FAFF8EEE
        FBFF86D7DEEF02798ABD011F2330000000000000000000000000000000000000
        000000000000000000000000000000353D4D3EA4B1CAB1F8FFFFA8F5FEFFA7F5
        FEFFB5F9FFFF8AD9DFEF02798ABD002025300000000000000000000000000000
        00000000000000000000000000000008090B017889AB017889AB017889AB0178
        89AB017889AB017889AB017889AB017889AB0000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000}
      OnClick = btPestodosClick
    end
    object btApagar: TSpeedButton
      Left = 619
      Top = 28
      Width = 25
      Height = 25
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000000000090000
        00130000001A00000019000000180000001600000014000000120000000F0000
        000D0000000A000000075E31049A381D025D0000000100000000000000110000
        00260303034804040466020202660101016600000066000000400000001E0000
        0019000000130000000E864A0BCE854A0ACD2E19045E00000000000000000606
        062F0D0D0D66DFDFDFFFCECECEFFCECECEFFCECECEFF0202026658340CAB9155
        13CC915513CC915513CC915513CCFFC538FF915513CC4227095C0D0D0D2F1818
        1866F0F0F0FFE1E1E1FFE1E1E1FFBFBFBFFFC0C0C0FFD1D1D1FF9D621DE0FFE3
        92FFFFD56AFFFFD15DFFFFD15DFFFFD15DFFFFD873FF9C611CCC23232366F0F0
        F0FFE3E3E3FFE3E3E3FFE3E3E3FFE3E3E3FFC2C2C2FFC4C4C4FFD2A571FFAA6E
        27E0A96D26D5A86C25CCA86C25CCFFE597FFA86C25CC4B30105C28282866F0F0
        F0FFE6E6E6FFE6E6E6FFE6E6E6FFE6E6E6FFE6E6E6FFC6C6C6FFC8C8C8FFD8D8
        D8FF101010660F0B072FB2762DCCB2762DCC5035145C000000001414142F2A2A
        2A66F1F1F1FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFE9E9E9FFCACACAFFCCCC
        CCFFDCDCDCFF16161666895844C55338165C0000000000000000000000001515
        152F2D2D2D66F3F3F3FFECECECFFECECECFFECECECFFECECECFFECECECFFCECE
        CEFFD0D0D0FF9F8EE3FF13015B95090128430000000000000000000000000000
        00001616162F2F2F2F66F4F4F4FFEFEFEFFFEFEFEFFFEFEFEFFFEFEFEFFFEFEF
        EFFF8E7DD2FF907FD4FFA392E7FF13015A920901284200000000000000000000
        0000000000001717172F32323266F6F6F6FFF2F2F2FFF2F2F2FFF2F2F2FFAE9D
        E8FFB09FE9FF9281D6FF9483D8FFA695EAFF13015A9009012941000000000000
        000000000000000000001919192F34343466F8F8F8FFF5F5F5FFAE9DE8FFB09F
        E9FFB2A1E9FFB3A2EAFF9584D9FF9786DBFFA998EDFF1301598D000000000000
        00000000000000000000000000001919192F36363666C1B0F9FFB09FE9FFB2A1
        E9FFB3A2EAFFB5A4EBFFB6A5ECFF9887DCFFAA99EEFF1301598A000000000000
        000000000000000000000000000000000000080027391201587FC4B3FBFFB3A2
        EAFFB5A4EBFFB6A5ECFFB8A7ECFFCAB9FEFF130159860800283D000000000000
        00000000000000000000000000000000000000000000080028391201577EC6B5
        FCFFB6A5ECFFB8A7ECFFCAB9FEFF120158810800283B00000000000000000000
        0000000000000000000000000000000000000000000000000000080027381201
        577CC9B8FDFFCAB9FEFF1201577E080027390000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000800
        27371201577B1201587C08002738000000000000000000000000}
      OnClick = btApagarClick
    end
    object edNome: TEdit
      Left = 6
      Top = 32
      Width = 179
      Height = 21
      TabOrder = 0
      OnKeyPress = edNomeKeyPress
    end
    object edNumero: TEdit
      Left = 204
      Top = 32
      Width = 115
      Height = 21
      TabOrder = 1
      OnKeyPress = edNumeroKeyPress
    end
    object cbTipo: TComboBox
      Left = 341
      Top = 32
      Width = 45
      Height = 21
      ItemHeight = 13
      TabOrder = 2
      Items.Strings = (
        '0'
        '1')
    end
    object btPesquisar: TButton
      Left = 679
      Top = 28
      Width = 79
      Height = 25
      Caption = 'Pesquisar'
      TabOrder = 3
      OnClick = btPesquisarClick
    end
  end
  object btCadastrar: TButton
    Left = 248
    Top = 465
    Width = 297
    Height = 45
    Align = alCustom
    Caption = 'Cadastrar Novo'
    TabOrder = 0
    OnClick = btCadastrarClick
  end
end
