object DM: TDM
  OldCreateOrder = False
  Height = 230
  Width = 335
  object sqlConn: TSQLConnection
    ConnectionName = 'MSSQLConnection'
    DriverName = 'MSSQL'
    GetDriverFunc = 'getSQLDriverMSSQL'
    LibraryName = 'dbxmss30.dll'
    LoginPrompt = False
    Params.Strings = (
      'SchemaOverride=sa.dbo'
      'DriverName=MSSQL'
      'HostName=NJTREI001'
      'DataBase=CaixaEletronico'
      'User_Name=sa'
      'Password='
      'BlobSize=-1'
      'ErrorResourceFile='
      'LocaleCode=0000'
      'MSSQL TransIsolation=ReadCommited'
      'OS Authentication=False'
      'Prepare SQL=False')
    VendorLib = 'oledb'
    Connected = True
    Left = 8
    Top = 16
  end
  object sqlUsuarios: TSQLDataSet
    SchemaName = 'sa'
    CommandText = 'Select * from Usuarios'
    DbxCommandType = 'Dbx.SQL'
    DataSource = dsUsuarios
    MaxBlobSize = -1
    Params = <>
    SQLConnection = sqlConn
    Left = 80
    Top = 16
    object sqlUsuariosCOD_User: TIntegerField
      FieldName = 'COD_User'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object sqlUsuariosNome: TStringField
      FieldName = 'Nome'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 50
    end
    object sqlUsuariosNumeroConta: TIntegerField
      FieldName = 'NumeroConta'
      ProviderFlags = [pfInUpdate]
    end
    object sqlUsuariosTipoUsuario: TStringField
      FieldName = 'TipoUsuario'
      ProviderFlags = [pfInUpdate]
      Required = True
      FixedChar = True
      Size = 1
    end
    object sqlUsuariosLogin: TStringField
      FieldName = 'Login'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 50
    end
    object sqlUsuariosSenha: TStringField
      FieldName = 'Senha'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 50
    end
    object sqlUsuariosSaldo: TIntegerField
      FieldName = 'Saldo'
      ProviderFlags = [pfInUpdate]
    end
  end
  object dpsUsuarios: TDataSetProvider
    DataSet = sqlUsuarios
    Options = [poAllowCommandText, poUseQuoteChar]
    Left = 72
    Top = 72
  end
  object cdsUsuarios: TClientDataSet
    Active = True
    Aggregates = <>
    Params = <>
    ProviderName = 'dpsUsuarios'
    OnNewRecord = cdsUsuariosNewRecord
    OnReconcileError = cdsUsuariosReconcileError
    Left = 80
    Top = 128
    object cdsUsuariosCOD_User: TIntegerField
      FieldName = 'COD_User'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsUsuariosNome: TStringField
      FieldName = 'Nome'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 50
    end
    object cdsUsuariosNumeroConta: TIntegerField
      FieldName = 'NumeroConta'
      ProviderFlags = [pfInUpdate]
    end
    object cdsUsuariosTipoUsuario: TStringField
      FieldName = 'TipoUsuario'
      ProviderFlags = [pfInUpdate]
      Required = True
      FixedChar = True
      Size = 1
    end
    object cdsUsuariosLogin: TStringField
      FieldName = 'Login'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 50
    end
    object cdsUsuariosSenha: TStringField
      FieldName = 'Senha'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 50
    end
    object cdsUsuariosSaldo: TIntegerField
      FieldName = 'Saldo'
      ProviderFlags = [pfInUpdate]
    end
  end
  object dsUsuarios: TDataSource
    DataSet = cdsUsuarios
    Left = 80
    Top = 176
  end
  object dsLogin: TDataSource
    DataSet = cdsLogin
    Left = 160
    Top = 176
  end
  object cdsLogin: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspLogin'
    Left = 160
    Top = 120
  end
  object dspLogin: TDataSetProvider
    DataSet = sqlLogin
    Left = 160
    Top = 72
  end
  object sqlLogin: TSQLDataSet
    CommandText = 'Select * from Usuarios'
    DbxCommandType = 'Dbx.SQL'
    DataSource = dsLogin
    MaxBlobSize = -1
    Params = <>
    SQLConnection = sqlConn
    Left = 160
    Top = 24
  end
  object DataSetProvider1: TDataSetProvider
    Left = 248
    Top = 104
  end
end
