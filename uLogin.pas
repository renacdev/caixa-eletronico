unit uLogin;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, dxGDIPlusClasses, udmDados;

var
  n: integer;

type
  TfmLogin = class(TForm)
    pn1: TPanel;
    pn2: TPanel;
    tlLogin: TLabel;
    Label1: TLabel;
    edUsuario: TEdit;
    Label2: TLabel;
    edSenha: TEdit;
    Image1: TImage;
    Image2: TImage;
    btEntrar: TButton;
    lbMessage: TLabel;
    procedure btEntrarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);

  private
    function getLogin(const Login, Senha:string) :boolean;
    function getTipoUsuario(const Login: string) : string;
  public
    { Public declarations }
  end;

var
  fmLogin: TfmLogin;

implementation

uses uAdmin, uUser, uClientes;
{$R *.dfm}

procedure TfmLogin.btEntrarClick(Sender: TObject);
begin
  if getLogin(edUsuario.Text, edSenha.Text) then
    if getTipoUsuario(edUsuario.Text) = '0' then
    begin
      lbMessage.Font.Color := clMenuBar;
      fmAdmin:=TfmAdmin.Create(Self);
    try
      fmAdmin.ShowModal;
    finally
      fmAdmin.Free
    end
  end
  else
  begin
    lbMessage.Font.Color := clMenuBar;
    fmUser:=TfmUser.Create(Self);
  try
    fmUser.ShowModal;
  finally
    fmUser.Free;
end;
  end
  else
    lbMessage.Font.Color := clred;
    edUsuario.Text:= '';
    edSenha.Text:='';
end;

procedure TfmLogin.FormShow(Sender: TObject);
begin
  lbMessage.Font.Color := clMenuBar;
  edUsuario.Text := '';
  edSenha.Text   := '';
  edUsuario.SetFocus;
end;

function TfmLogin.getLogin(const Login, Senha: string): boolean;
begin
  with DM.sqlConn, DM.sqlLogin do
  begin
    close;
    CommandText:= 'select Cod_User from Usuarios '+
    'where (login = '+ QuotedStr(login) + ' )and( senha = '+ quotedStr(senha)+ ')';
    open;
  if Fields[0].AsInteger > 0 then
    Result:= true;
  end;
 end;

function TfmLogin.getTipoUsuario(const Login: string) : string;
begin
  with DM.sqlConn, DM.sqlLogin do
  begin
    close;
    CommandText:= 'select TipoUsuario from Usuarios ' +
    'where login = '+QuotedStr(login);
    open;
    result:= fields[0].AsString;
  end;
end;

end.
