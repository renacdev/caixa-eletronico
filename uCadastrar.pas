unit uCadastrar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Mask, DBCtrls;

type
  TfmCadastrar = class(TForm)
    Image1: TImage;
    rbAdmin: TRadioButton;
    rbUser: TRadioButton;
    lbTitulo: TLabel;
    Label1: TLabel;
    lbNome: TLabel;
    lbUsuario: TLabel;
    lbLogin: TLabel;
    lbSenha: TLabel;
    btnCadastrar: TButton;
    edNome: TDBEdit;
    edLogin: TDBEdit;
    edSenha: TDBEdit;
    edTipoUsuario: TDBEdit;
    edNumeroConta: TDBEdit;
    edSaldo: TDBEdit;
    procedure btnCadastrarClick(Sender: TObject);
    procedure edNomeKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);

  private
    function validarLogin(const Login : String) :boolean;
  public
    { Public declarations }
  end;

var
  fmCadastrar: TfmCadastrar;

implementation

{$R *.dfm}

uses uLogin, uClientes, udmDados, uListar;

function  Ret_Numero(Key: Char; Texto: string; EhDecimal: Boolean = False): Char;
begin
  if  not EhDecimal then
    begin
      if  not ( Key in ['0'..'9', Chr(8)] ) then
          Key := #0
    end
  else
    begin
      if  Key = #46 then
          Key := DecimalSeparator;
      if  not ( Key in ['0'..'9', Chr(8), DecimalSeparator] ) then
          Key := #0
      else if  ( Key = DecimalSeparator ) and ( Pos( Key, Texto ) > 0 ) then
          Key := #0;
    end;
     Result := Key;
end;

procedure TfmCadastrar.btnCadastrarClick(Sender: TObject);
begin
Randomize;
//Valida��o campos em branco
  if edNome.Text = '' then
  begin
    showMessage('Digite um nome');
    edNome.SetFocus;
    exit;
  end;
  if edLogin.Text = '' then
  begin
    showMessage('Digite o login');
    edLogin.SetFocus;
    exit;
  end;
  if edSenha.Text = '' then
  begin
    showMessage('Digite uma senha');
    edSenha.SetFocus;
    exit;
  end;
  if rbAdmin.checked or rbUser.checked = false then
  begin
   showMessage('Informe o tipo de usu�rio');
   rbAdmin.SetFocus;;
   exit;
  end;
//*************************************************

  if rbAdmin.checked = true then
    edTipoUsuario.Text:= '0'
  else if rbUser.checked = true then
    edTipoUsuario.Text:= '1';

  if rbAdmin.checked = true then
    edNumeroConta.Text:= ''  //Admin n�o recebe n�mero de conta
  else if rbUser.checked = true then
    edNumeroConta.Text:=(IntToStr(50000 + Random(10000))); //Gerar n�mero para usu�rio comum

  if rbAdmin.checked = true then
    edSaldo.Text:= '' //Admin n�o recebe saldo
  else if rbUser.checked = true then
    edSaldo.Text:= (IntToStr(1000 + Random(1000))); //Gerar saldo automaticamente para usu�rio comum

//***********************************************************

  if validarLogin(edLogin.Text) = true then
  begin
    showmessage('Login j� existente');
    edLogin.Text:= '';
    edLogin.SetFocus;
    exit;
  end

  else
  DM.cdsUsuarios.FetchParams;
  DM.cdsUsuarios.ApplyUpdates(0);
  DM.cdsUsuarios.refresh; //Listar mais de um usu�rio - Resolve o Key Violation

  if DM.cdsUsuarios.ApplyUpdates(0) = 0 then
    showMessage('Usu�rio Cadastrado')
  else
    showMessage('ERRO DE CONEX�O');
    fmListar.visible:= true;
    close;
end;

procedure TfmCadastrar.edNomeKeyPress(Sender: TObject; var Key: Char);
begin
 if Key in ['0'..'9'] then
     Key := #0;
end;

procedure TfmCadastrar.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   try
    fmListar:=TfmListar.Create(Self);
  finally
    fmListar.ShowModal;
  end;
   fmListar.visible:= true;
   dm.cdsUsuarios.Close;
   dm.cdsUsuarios.Open;
end;

procedure TfmCadastrar.FormShow(Sender: TObject);
begin
  fmListar.visible:= false;
  if wg_bt_func = 0  then
  begin
    DM.cdsUsuarios.Append;
    DM.cdsUsuarios.Insert;
  end;
    if wg_bt_func = 1  then
  begin
    DM.cdsUsuarios.Edit;
  end;
end;

function TfmCadastrar.validarLogin(const Login: String): boolean;
begin
  with DM.sqlConn, DM.sqlLogin do
  begin
    close;
    CommandText:= 'select Login from Usuarios ' +
    'where Login = '+quotedStr(edLogin.Text);
    open;
    result:= fields[0].AsBoolean;
  end;
end;

end.
