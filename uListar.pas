unit uListar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, uCadastrar, uClientes, udmDados, Grids, DBGrids, ExtCtrls,
  DBCtrls, Buttons;

type
  TfmListar = class(TForm)
    btCadastrar: TButton;
    DBGrid1: TDBGrid;
    btEditar: TSpeedButton;
    btDelete: TSpeedButton;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    edNome: TEdit;
    edNumero: TEdit;
    cbTipo: TComboBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    btPestodos: TSpeedButton;
    Button1: TButton;
    btPesquisar: TButton;
    btApagar: TSpeedButton;
    procedure btCadastrarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btEditarClick(Sender: TObject);
    procedure btDeleteClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure edNomeKeyPress(Sender: TObject; var Key: Char);
    procedure edNumeroKeyPress(Sender: TObject; var Key: Char);
    procedure btApagarClick(Sender: TObject);
    procedure btPestodosClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
  private
    { Public declarations }
  public
    { Public declarations }
  end;

var
  fmListar: TfmListar;
  wg_bt_func : integer;

implementation

{$R *.dfm}

procedure TfmListar.btCadastrarClick(Sender: TObject);
begin
  wg_bt_func:= 0;
   try
    fmCadastrar:=TfmCadastrar.Create(Self);
  finally
    fmCadastrar.ShowModal;
  end;
    fmCadastrar.Free;
end;

procedure TfmListar.btDeleteClick(Sender: TObject);
begin
  if Application.MessageBox(
     'Deseja excluir o usu�rio?',
     'Confirme',
     MB_YESNO+MB_SYSTEMMODAL+MB_ICONQUESTION+MB_DEFBUTTON1) = ID_YES then
  begin
    DM.cdsUsuarios.Delete;
    DM.cdsUsuarios.ApplyUpdates(0);
    DM.cdsUsuarios.refresh;
    showmessage('Usuario deletado');
  end
  else
    exit;
end;

procedure TfmListar.btEditarClick(Sender: TObject);
begin
    wg_bt_func:= 1;
   try
    fmCadastrar:=TfmCadastrar.Create(Self);
  finally
    fmCadastrar.ShowModal;
  end;
    fmCadastrar.Free;
end;

procedure TfmListar.btPesquisarClick(Sender: TObject);
begin
  if (edNome.Text = '') or (edNumero.Text = '') or (cbTipo.Text = '') then
    begin
      with DM.sqlConn, DM.cdsUsuarios do
        begin
          close;
          CommandText:= 'select * from Usuarios ';
          open;
        end;
    end;

  if edNome.Text <> '' then
    begin
      with DM.sqlConn, DM.cdsUsuarios do
        begin
          close;
          CommandText:= 'select * from Usuarios '+
          'where Nome like''%'+ edNome.Text+'%''';
          open;
        end;
    end;

  if edNumero.Text <> '' then
  begin
    with DM.sqlConn, DM.cdsUsuarios do
      begin
        close;
        CommandText:= 'select * from Usuarios '+
        'where NumeroConta = '+QuotedStr(edNumero.Text) ;
        open;
        edNumero.Tag := 0;
        edNome.ReadOnly := false;
        cbTipo.enabled := true;
        edNumero.Text := '';
      end;
  end;

  if cbTipo.Text <> '' then
  begin
    with DM.sqlConn, DM.cdsUsuarios do
      begin
        close;
        CommandText:= 'select * from Usuarios '+
        'where TipoUsuario = '+QuotedStr(cbTipo.Text) ;
        open;
      end;
  end;

  if (edNome.Text <> '') and ( cbTipo.Text <> '') then
  begin
    with DM.sqlConn, DM.cdsUsuarios do
      begin
        close;
        CommandText:= 'select * from Usuarios '+
        'where Nome = '+QuotedStr(edNome.Text)+
        ' and TipoUsuario = ' +QuotedStr(cbTipo.Text) ;
        open;
      end;
  end;

end;

procedure TfmListar.edNomeKeyPress(Sender: TObject; var Key: Char);
begin
  if Key in ['0'..'9'] then
    Key := #0;
end;

procedure TfmListar.edNumeroKeyPress(Sender: TObject; var Key: Char);
begin
  edNome.Text := '';
  cbTipo.Text := '';
  if Key in ['a'..'z'] then
    Key := #0;

  if edNumero.Text <> '' then
    begin
      edNumero.Tag <> 0;
      edNome.Text := '';
      edNome.ReadOnly := true;
      cbTipo.enabled := false;
    end;
  if edNumero.Tag <> 0 then
    begin
      edNome.Text := '';
      cbTipo.Text := '';
    end;
end;

procedure TfmListar.FormShow(Sender: TObject);
begin
  DM.cdsUsuarios.open;
end;

procedure TfmListar.btPestodosClick(Sender: TObject);
begin
  with DM.sqlConn, DM.cdsUsuarios do
  begin
    close;
    CommandText:= 'select * from Usuarios ';
    open;
    edNome.Text := '';
    edNumero.Text := '';
    cbTipo.Text := '';
    edNumero.Tag := 0;
    edNome.ReadOnly := false;
    cbTipo.enabled := true;
  end;
end;

procedure TfmListar.DBGrid1DblClick(Sender: TObject);
begin
      wg_bt_func:= 1;
   try
    fmCadastrar:=TfmCadastrar.Create(Self);
  finally
    fmCadastrar.ShowModal;
  end;
    fmCadastrar.Free;
end;

procedure TfmListar.btApagarClick(Sender: TObject);
begin
  edNome.Text := '';
  edNumero.Text := '';
  cbTipo.Text := '';
  edNome.SetFocus;
  edNumero.Tag := 0;
  edNome.ReadOnly := false;
  cbTipo.enabled := true;
end;

end.
