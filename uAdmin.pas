unit uAdmin;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, uCadastrar, uListar;

type
  TfmAdmin = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    btCadastrar: TButton;
    btInserir: TButton;
    btExibir: TButton;
    btSair: TButton;
    procedure btSairClick(Sender: TObject);
    procedure btCadastrarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btInserirClick(Sender: TObject);
    procedure btExibirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAdmin: TfmAdmin;

implementation

uses uLogin, uInserir;

{$R *.dfm}

procedure TfmAdmin.btCadastrarClick(Sender: TObject);
begin
  try
    fmListar:=TfmListar.Create(Self);
  finally
    fmListar.ShowModal;
  end;
    fmListar.Free;
end;

procedure TfmAdmin.btExibirClick(Sender: TObject);

begin
  showmessage(
  'Notas disponíveis: ' + (IntToStr(wg_ed_quant))
  +#13
  +'Saldo: ' +(IntToStr(wg_saldo))
  )
end;

procedure TfmAdmin.btInserirClick(Sender: TObject);
begin
  fmInserir:=TfmInserir.Create(Self);
  try
  fmInserir.ShowModal;
  finally
  fmInserir.Free;
  end;
end;

procedure TfmAdmin.btSairClick(Sender: TObject);
begin
  fmAdmin.Close;
  fmLogin.Show;
end;
procedure TfmAdmin.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  fmLogin.Show;
end;

end.
